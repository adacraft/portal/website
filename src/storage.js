import axios from 'axios'
import { adacraftStorageUrl } from './config'

export const getProjectUrl = (project) => {
  const storageUrl = project.storage
  let url = storageUrl
  if (storageUrl === 'https://storage.adacraft.org/2') {
    // This project is stored in the second storage container. For now,
    // we don't use the base URL from the project description but we
    // compute it based on the current configuration of the editor. This
    // allows us to have separate storage spaces for different
    // deployment environment (prod, staging, beta...) even with a
    // shared project database.
    //
    // So, basicly, we convert from something like
    // "https://storage.beta.adacraft.org" to something like
    // "https://storage2.beta.adacraft.org"
    url = adacraftStorageUrl.replace('storage.', 'storage2.')
  }
  const projectUrl = `${url}/projects/${project.id}`
  return projectUrl
}

export const getProjectLastModification = async (project) => {
  const projectUrl = getProjectUrl(project)
  const data = await axios.head(projectUrl)
  return data.headers['last-modified']
}