let atLeastOneEnvVarIsMissing = false
const checkEnvVarIsNotUndefined = (value, envVarName) => {
  if (value === undefined) {
    console.error(`This project build misses some settings. The following environment variable is missing: ${envVarName}. Be sure to set it (in .env.local file for example) and rebuild the project.`)
    atLeastOneEnvVarIsMissing = true
  }
}

// @ts-ignore
export const faunaPublicKey = import.meta.env.VITE_FAUNA_PUBLIC_KEY
checkEnvVarIsNotUndefined(faunaPublicKey, 'VITE_FAUNA_PUBLIC_KEY')
// @ts-ignore
export const goTrueApiUrl = import.meta.env.VITE_GOTRUE_API_URL
checkEnvVarIsNotUndefined(goTrueApiUrl, 'VITE_GOTRUE_API_URL')
// @ts-ignore
export const adacraftStorageUrl = import.meta.env.VITE_ADACRAFT_STORAGE_URL
checkEnvVarIsNotUndefined(adacraftStorageUrl, 'VITE_ADACRAFT_STORAGE_URL')

if (atLeastOneEnvVarIsMissing) {
  alert('There are some missing settings for this build. See the console for more information.')
}

// Get the public base path. This path can be different between deployment
// environments. We use vite to build the project which provides the base URL in
// BASE_URL env var (see https://vitejs.dev/guide/build.html#public-base-path.)
//
// @ts-ignore
export const baseUrl = import.meta.env.BASE_URL