import moment from 'moment'

/**
 * @param {array} array 
 */
export const shuffle = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1)); //random index
    [array[i], array[j]] = [array[j], array[i]]; // swap
  };
};

// Get 'username' from anything like 'username' or
// "http://any.where.org/username" or "http://any.where.org/username/" (with
// a trailing slash).
export const parseUser = (string) => {
  return string
    // Remove trailing slah if any
    .replace(/\/$/, '')
    // Extract last part
    .split('/').pop()
    // Remove optional leading "@"
    .replace(/^@+/, '')
}

export const getLocalDate = (date) => moment.utc(date).local()
