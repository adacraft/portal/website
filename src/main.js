import { createApp, nextTick } from 'vue'
import { createI18n } from 'vue-i18n'

import App from './App.vue'
import router from './router'
import auth from './auth.js'
import { useStore } from './store.js'
import faunadb from 'faunadb'

import './assets/styles/main.css'

import en from './locales/en.json5'
import fr from './locales/fr.json5'

// The recovery token is stored inn the URL as something like that:
//    https://adacraft.org/#recovery_token=sFd-xut8uflMBiXbIrQjTw
const recoveryToken = decodeURIComponent(document.location.hash).split(
  'recovery_token='
)[1]

if (recoveryToken !== undefined) {
  auth.settings
  auth.recover(recoveryToken)
    .then((user) => {
      router.push('change-password-reset')
    })
    .catch((error) => {
      console.log(`Can't recover password, error: ${error}`)
    })
}

const { authentification } = useStore()
const user = auth.currentUser()
if (user) {
  authentification.user = user.user_metadata.full_name
  authentification.netlifyUser = user

  const client = new faunadb.Client({
    secret: authentification.netlifyUser.app_metadata.db_token
  })
  const q = faunadb.query
  client
    .query(q.Identity())
    .then((result) => {
      const userId = result.value.id
      client
        .query(
          q.Get(q.Ref(q.Collection('users'), userId))
        )
        .then((result) => {
          authentification.faunadbUser = result
        })
    })
}

function getBrowserLocale() {
  const navigatorLocale =
    navigator.languages !== undefined
      ? navigator.languages[0]
      : navigator.language
  if (!navigatorLocale) {
    return undefined
  }
  // from "en-US" to "en"
  const trimmedLocale = navigatorLocale
    .trim()
    .split(/-|_/)[0]
  return trimmedLocale
}

function getStartingLocale() {
  const browserLocale = getBrowserLocale()
  const supportedLocales = ['en', 'fr']
  let locale
  if (supportedLocales.includes(browserLocale)) {
    locale = browserLocale
  } else {
    locale = 'en'
  }
  return locale
}

const i18n = createI18n({
  locale: getStartingLocale(),
  fallbackLocale: 'en',
  messages: {
    en,
    fr
  }
})

const app = createApp(App)
  .directive('focus', {
    mounted(el, binding, vnode) {
      nextTick(() => {
        el.focus()
      })
    }
  })
  .use(router)
  .use(i18n)
  .mount('#app')
