const faunadb = require('faunadb')

const q = faunadb.query

console.log('Create the database...')

if (!process.env.FAUNADB_SERVER_SECRET) {
  console.log(
    'FAUNADB_SERVER_SECRET env var is needed to create the database'
    )
  process.exit(1);
}

const key = process.env.FAUNADB_SERVER_SECRET;
const client = new faunadb.Client({
  secret: key
})

client
  .query(
    q.CreateCollection({
      name: 'users',
      permissions: {
        read: "public"
      }
})
  )
  .then(() =>
    client.query(
      q.Do(
        q.CreateCollection({
          name: "projects",
          permissions: {
            create: q.Collection("users"),
            read: "public"
          }
        }),
        q.CreateCollection({
          name: "projects_codes",
          permissions: {
            create: q.Collection("users"),
            read: "public"
          }
        })
      )
    )
  )
  .then(() =>
    client.query(
      q.Do(
        q.CreateIndex({
          name: 'users_by_id',
          source: q.Collection('users'),
          terms: [
            {
              field: ['data', 'id']
            }
          ],
          unique: true
        }),
        q.CreateIndex({
          name: "all_projects",
          source: q.Collection("projects"),
          permissions: {
            read: "public"
          }
        }),
        q.CreateIndex({
          name: "all_projects_codes",
          source: q.Collection("projects_codes"),
          permissions: {
            read: "public"
          }
        }),
        q.CreateIndex({
          name: "projects_search_by_user",
          source: q.Collection("projects"),
          terms: [
            {
              field: ["data", "owner"]
            }
          ],
          permissions: {
            read: "public"
          }
        })
    )
    )
  )
  .then(() => {
    console.log('The database has been successfully created');
  })
  .catch(error => {
    if (error.message === 'instance already exists') {
      console.log('The database is already created: there is nothing to do')
      process.exit(0)
    } else {
      console.error('There was a problem whil creating the database:', error)
      throw error
    }
  })
