const faunadb = require('faunadb')

const q = faunadb.query

console.log('Update the database...')

if (!process.env.FAUNADB_SERVER_SECRET) {
  console.log(
    'FAUNADB_SERVER_SECRET env var is needed to create the database'
    )
  process.exit(1);
}

const key = process.env.FAUNADB_SERVER_SECRET;
const client = new faunadb.Client({
  secret: key
})

client
  .query(
    q.CreateIndex({
      name: "unique_projects_id",
      source: q.Collection("projects"),
      terms: [
        {
          field: ["data", "id"]
        }
      ],
      permissions: {
        read: "public"
      },
      unique: true
    })
  )
  .then(() => {
    console.log('The database has been successfully updated');
  })
  .catch(error => {
    console.error('There was a problem while updating the database: ', error)
    throw error
  })
