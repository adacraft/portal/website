module.exports = {
  darkMode: 'class',
  content: [
    './index.html',
    './src/**/*.vue',
    './src/**/*.js',
  ],
  theme: {
    extend: {
      width: {
        '37.5': '9.375rem',
        '92.5': '23.125rem',
        '125': '31.25rem'
      },
      height: {
        '37.5': '9.375rem',
        '45': '11.25rem',
        '80.5': '20.125rem'
      },
      spacing: {
        '40': '10rem'
      },
      fontFamily: {
        'ada': ['Averia Libre', 'Segoe UI']
      },
    },
  },
  variants: {},
  plugins: [],
}
