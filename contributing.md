# How to contribute?

Thank you for contributing to adacraft! This document will guide you through the
workflow and best practices you need to know to send your contribution.

Our main workflow uses GitLab for source control, issues, and merge requests
management.

## Reporting bugs

If you have found a bug, you can first search the existing issues to see if it
has already been reported.

If it's a new bug, please open a new issue. Remember to add as much information
as possible.

## Code contribution

Everyone can submit contribution to the code. It's a bit different depending
you're a member of the adacraft team (i.e. with the GitLab role "Developer") or
not.  

### External contributions

If you're not a member of the adacraft team, use the classical GitLab merge
requests. Fork the repository, create a feature branch, commit your changes,
push the feature branch to the fork, then send a merge request to the original.

### adacraft team contributions

The team uses [Gitlab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
to manage contributions. Essentially, it consists of first creating an issue for
the contribution, create a merge request from it (which will create a new
branch), fetch and checkout the new branch, work on it, commit, push, and mark
the merge request as ready.

### Commit message

The adacraft team follows these [commit guidlines](https://cbea.ms/git-commit/).

In short: to write a good commit message, make it short (less than 50
characters) and descriptive. If you need to write more, do it after a blank
line.